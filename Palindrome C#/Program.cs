﻿using System;

//C# Code used from https://www.w3resource.com/csharp-exercises/recursion/csharp-recursion-exercise-8.php

public class Exercise
{
       public static bool IsPalindrome(string Text)
        {
            if (Text.Length <= 1)
                return true;

        //Recursion====================================================
        //else
        //{
        //    if (Text[0] != Text[Text.Length - 1])
        //        return false;
        //    else
        //        return IsPalindrome(Text.Substring(1, Text.Length - 2));
        //}
        //================================================================

        //Iteration====================================================
        else
        {
                char left, right;
                for(int i = 0; i <= Text.Length / 2;)
                {
                    left = Text[i];
                    right = Text[Text.Length - i - 1];
                    i++;
                    if (left != right)
                        return false;
                    else
                        return true;
                }
                return false;
            }
        //====================================================
        }

        public static void Main()
        {
            Console.WriteLine("Recurision: Check Weather a string is a Palindrome or not: \n");
            string str1;
            bool tf;

            Console.Write("Input a String: ");
            str1 = Console.ReadLine();
            tf = IsPalindrome(str1.ToLower());

            if (tf)
                Console.WriteLine("The String is a Palindrome");
            else
                Console.WriteLine("The String is not a Palindrome");
        }
}
