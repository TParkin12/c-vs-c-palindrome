// C++ Conversion Example program
#include <iostream>
#include <string>

using namespace std;

static bool IsPalindrome(const std::string& Text)
{
    ///Recursion
   /* if (Text.length() <= 1)
        return true;
    else
    {
        if (Text[0] != Text[Text.length() - 1])
            return false;
        else
            return IsPalindrome(Text.substr(1, Text.length() - 2));
    }*/


    ///Iteration
    if (Text.length() <= 1)
        return true;

    char left, Right;
    for (size_t i = 0; i <= Text.length() / 2; i++)
    {
        left = Text[i];
        Right = Text[Text.length() - i - 1];

        if (left != Right)
            return false;
        else
            return true;
    }
    return false;
}

int main()
{
    std::cout << "Recurision: Check Weather a string is a Palindrome or not: \n";
    std::string str1 = "";
    bool tf = false;

    std::cout << "\nInput a string: ";
    cin >> str1;

    for (auto & c : str1)
        c = std::tolower(c);

    tf = IsPalindrome(str1);

    if (tf)
        cout << "\nThe String is a Palindrome";
    else
        cout << "\nThe String is not a Palindrome\n";
    return 0;
}
